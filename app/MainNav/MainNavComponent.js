angular
  .module('myApp')
  .component('mainNavComponent',{
    templateUrl: 'MainNav/MainNavComponent.html',
    controller: function () {

      /*this.buttons = { // при задании через объект не гарантирован порядок св-в (в случае, когда св-во было удалено из объекта)
        list: 'Список',   // в этом случае JS полагается на порядок вывода св-в браузером for key in myObj
        createNewPokemon: 'Создать нового',  // https://docs.angularjs.org/api/ng/directive/ngRepeat
        myAccount: 'Личный кабинет'
      }*/
      this.buttons = [ // при задании через массив порядок элементов гарантирован JS
        {sref: 'list', title: 'Список'},
        {sref: 'createNewPokemon', title: 'Создать нового'},
        {sref: 'myAccount', title: 'Личный кабинет'}
      ]
    }
  });
