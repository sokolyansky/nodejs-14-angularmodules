'use strict';

angular
  .module('myApp')
  .factory('MyAccountStore', function () {
    let myData ={};

    return {
      getData() {
        return myData;
      },
      addData(info) {
        myData = {
          name: info.name,
          email: info.email,
          phone: info.phone || 5
        }
      }
    }
  });
